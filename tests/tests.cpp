#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "Fib.h"

TEST_CASE("Fib Test"){
	REQUIRE(fib(1) == 1);
	REQUIRE(fib(2) == 5);
}